import 'reflect-metadata';
import config from './config';
import express from 'express';

import Logger from './loaders/loggers';

// checking in build file ...//
// checking again..//

async function startServer() {

    const app = express();

    await require('./loaders').default({ expressApp : app });

    app.listen(config.port, () => {
        // if(err){
        //     process.exit(1);
        //     return;
        // }
        Logger.info(`
        #############################################
          Server Listening on port : ${config.port}
        #############################################
         `);
    })
}

startServer();

