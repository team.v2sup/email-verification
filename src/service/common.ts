import { NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import config from '../config';


const verifyToken = (req : any , res : any , next : NextFunction) => {
    try{
        console.log(req.headers, " this is req.headers from common file...");

        if(!req.headers.authorization){
            throw new Error('Unauthorized APIs');
        }

        const { authorization } = req.headers as any;
        
        const token : string = authorization.split(" ")[1];

        if(!token) throw new Error('Unauthorized API');

        jwt.verify(token , config.jwtSecret , (err : any , result : any) => {
            if(err) throw new Error(err)

            req.user = { ...result }
        })

        next()

    }catch(e){
        return res.status(400).json({
            status : false,
            message : e.message,
            data : null
        })
    }
}


const sendingEmail = (subject : string , email : any , url : string) => {
    console.log('inside sending email function');

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: config.emailId,
          pass: config.password
      }
    });

    const mailOptions = {
        from: config.emailId, // sender address
        to: email , // list of receivers
        subject: `${subject} mail`, // Subject line
        text: `${url}`// plain text body
    };

    transporter.sendMail(mailOptions, function (err : any, info: any) {
      if(err)
          console.log(err)
      else
          console.log(info);
    })
    console.log('email sent');
}

export {
    sendingEmail,
    verifyToken
}