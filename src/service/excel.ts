import { Response } from 'express';
import xlsx from 'xlsx';
import path from 'path';
import fs from 'fs';
import { v4 as uuidv4 } from 'uuid';

const excelData = (req: any) => {

    const fileName = req.files.importExcel.name as any;

    const fileData = req.files.importExcel.data;

    const wb = xlsx.read(fileData, { type: "buffer" })

    const sheets = wb.SheetNames

    const data: any = xlsx.utils.sheet_to_json(wb.Sheets[sheets[0]]);

    return data;
}

const downloadExcel = (excelArray: any) => {

    const newWb = xlsx.utils.book_new();

    const newWs = xlsx.utils.json_to_sheet(excelArray);

    xlsx.utils.book_append_sheet(newWb, newWs, "userDataExcel")

    let fileN = uuidv4() + '.xlsx'

    let filePath = path.join(__dirname, './excelFiles/', fileN)

    xlsx.writeFile(newWb, filePath)

    return { filePath, fileN }

}

export {
    excelData,
    downloadExcel
}