import { NextFunction } from 'express';

const getXuser = (req : any , res : any , next : NextFunction) => {
    try{
       
        console.log(req.headers);
        console.log(req.headers['x-user']);
        const user = JSON.parse(req.headers['x-user']);
        

        if(typeof JSON.parse(req.headers['x-user']) === 'object'){
            req.user = user
            next()
        }else{
            return res.status(401).json({
                status : false,
                message : 'Unauthorised',
                data : null
            })
        }
    }catch(e){
        console.log(e);
        res.status(400).json({
            status : false,
            message : e.message,
            data : null
        })
    }
}

export default {
    getXuser
}