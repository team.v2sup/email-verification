import { NextFunction, Request, Response } from 'express';
const Joi = require('joi')

const addImportValidate = (req: any, res: Response, next: NextFunction) => {
    if (!req.files || Object.keys(req.files).length === 0 || !['xlsx', 'xls'].includes((req.files.importExcel.name).split('.')[1])) {
        return returnResponse(res, new Error('No Excel file attached'));
    }

    next()
}

const jsonExcelValidate = (req: Request, res: Response, next: NextFunction) => {

    let { error } = Joi.object().keys({
        excelArray: Joi.array().items(Joi.object().keys({
            email: Joi.string(),
            exist: Joi.boolean()
        })).required()
    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const excelFieldValidate = (req: Request, res: Response, next: NextFunction) => {
    console.log('inside excelfield')
    console.log(req.body)
    let { error } = Joi.object({

        subject: Joi.string().required()

    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const importJobFieldValidate = (data: any) => {

    console.log('inside importJobValidate')
    let { error } = Joi.array().items(Joi.object({

        email: Joi.string().email().required()

    })).validate(data)

    if (error) throw (error.details[0])

}

const verifyEmailIdValidate = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({
        emailId: Joi.string().email().required()
    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const sendSimpleEmailValidate = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({
        emailId: Joi.string().email().required(),
        txtMessage: Joi.string().required().allow(''),
        htmlMessage: Joi.any().optional().allow(''),
        subject: Joi.string().required(),
        source: Joi.string().email().required(),
    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const sendBulkEmailValidate = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({
        emailIds: Joi.array().items(Joi.string().required()),
        templateName: Joi.string().required(),
        source: Joi.string().email().required(),
        replacementTemplateData: Joi.array().items(Joi.object().keys({
            key: Joi.string().required(),
            value: Joi.string().required(),
        }))
    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const emailExistSingleValidate = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({

        email: Joi.string().email().required(),

    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const sendBulkEmailValidate2 = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({

        templateName: Joi.string().required(),
        source: Joi.string().email().required(),

    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}

const deleteEmailIdValidate = (req: Request, res: Response, next: NextFunction) => {
    let { error } = Joi.object().keys({
        emailId: Joi.string().email().required()
    }).validate(req.body);

    if (error) return returnResponse(res, error.details[0].message);

    next()
}


function returnResponse(res: Response, error: Error) {
    return res.status(400).json({
        status: false,
        message: error.message || error,
        data: null
    })
}


export default {
    sendSimpleEmailValidate,
    deleteEmailIdValidate,
    verifyEmailIdValidate,
    sendBulkEmailValidate,
    addImportValidate,
    importJobFieldValidate,
    excelFieldValidate,
    emailExistSingleValidate,
    jsonExcelValidate
};
