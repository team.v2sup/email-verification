
import verify from './verify';
import emailValidate from './email';


export default {
  ...verify,
  ...emailValidate
};
