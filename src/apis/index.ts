import { Router } from 'express';
import gateway from './routes/gateway';
import email from './routes/email';

export default () => {
  const app = Router();
  email(app);
  gateway(app);

  return app;
};