


/**
* @swagger
* /email/emailExistSingle:
*   post:
*     tags:
*       - Email APIs
*     summary: Verify Single Email
*     description: Verify Single Email
*     produces:
*      - application/json
*     parameters:
*      - name: body
*        description: body
*        in: body
*        required: true
*        schema:
*          type: object
*          required: true
*          properties:
*            email:
*              type: string
*              required: true
*              example: "sahib@gmail.com"
*     responses:
*       200:
*         description: Success
*       400:
*         description: Bad Request/Parameters
*       401:
*         description: Unauthorized
*       404:
*         description: Invalid url or Resource not found
*       500:
*         description: Internal server error
*/

/**
 * @swagger
 * /email/verify-be:
 *   post:
 *     tags:
 *      -  Email APIs
 *     summary: Import Excel File
 *     produces:
 *      - application/json
 *     consumes:
 *      - multipart/form-data
 *     parameters:
 *      - in: formData
 *        name: importExcel
 *        type: file
 *        description: Contains email Ids.
 *      - in: formData
 *        name: emailId
 *        type: string
 *        example: 'sahib90.41@gmail.com,developer@v2support.com'
 *     responses:
 *       200:
 *         description: Sucess
 *       400:
 *         description: Bad Request/Parameters
 *       401:
 *         description: Unauthorized
 *       404:
 *         description: Invalid Url or Resource Not Found
 *       500:
 *         description: Internal Server Error
 */
