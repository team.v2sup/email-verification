import { Router } from 'express';
import config from '../../config';

export default () => {
    const app = Router();

    if (config.nodeEnv === 'development') {
        const swaggerJSDoc = require('swagger-jsdoc');
        const swaggerUi = require('swagger-ui-express');

        let options = {
            definition: {
                info: {
                    title: "Email Service",
                    version: '1.0.0'
                },
                basePath: '/notification'
            },
            apis: [
                './src/apis/swagger/email/swagger.js',
                './src/apis/swagger/template/swagger.js'
            ]
        };

        let swaggerSpec = swaggerJSDoc(options);
        app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec, {
            customCss: '.swagger-ui .topbar { display : none }'
        }));

    }

    return app;
};