import { Router , Request , Response } from 'express';
import { verifyToken } from '../../service/common';

const route = Router();

export default (app : Router) => {
    app.use('/gateway' , route);

    route.get('/' , verifyToken , (req : any , res : Response) => {
        try{
            if(req.user.isDeleted){
                return returnResponse(401 , res , 'Deleted by Admin, Please contact support');
            }

            return returnResponse(200 , res , 'Success');
        }catch(e){
            return returnResponse(400 , res , e.message);
        }
    })
}

const returnResponse = (statusCode : number , res : Response , err : string) => {
    return res.status(statusCode).json({
        status : false,
        data : null,
        message : err
    })
}