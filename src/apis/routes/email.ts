const emailExistence = require('email-existence');
import { Router, Response } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares/email'
import { excelData, downloadExcel } from '../../service/excel';
let nodemailer = require("nodemailer");
const EmailValidator = require('email-deep-validator');

const route = Router();

export default (app: Router) => {
    app.use('/email', route);

    route.post('/verify-be', middlewares.addImportValidate, async (req: any, res: Response) => {

        const logger = Container.get('logger') as any;

        try {

            const data = excelData(req);

            middlewares.importJobFieldValidate(data)

            sendEmailResp(data, req.body.emailId);

            return res.status(200).json({
                status: true,
                message: 'You will recieve an email in a while !',
                data: {}
            })


        } catch (e) {

            logger.error(' error : %o', e);

            return res.status(400).json({
                status: false,
                message: e.message
            })
        }
    })

    async function sendEmailResp(data: any, emailId: string) {

        let excelArray: any = [];

        let invalidEmails: any = [];

        await checkEmail(data, excelArray, invalidEmails)

        excelArray.push(...invalidEmails);

        const { filePath, fileN } = downloadExcel(excelArray);

        const params = {

            from: '"V2Support 👻" <developer@v2support.com>',
            to: emailId.split(','),
            text: 'Thanks for using our email verification service.',
            subject: 'Your email verified successdully!',
            attachments: [
                {
                    path: filePath

                }
            ]
        };

        //Send Email 

    }

    function checkEmail(emails: any[], valid: any, invalid: any) {

        return new Promise((resolve, reject) => {

            try {

                if (emails.length === 0) {
                    return resolve(0)
                }

                let emailId = emails[0]['email'];



                const emailValidator = new EmailValidator();
                emailValidator.verify(emailId)
                    .then((resp: any) => {
                        console.log(resp);

                        if (resp.wellFormed && resp.validDomain) {
                            console.log(emailId, '1')
                            valid.push({ email: emailId, wellFormed: resp.wellFormed, validDomain: resp.validDomain, validMailbox: resp.validMailbox })
                        } else {
                            invalid.push({ email: emailId, exist: false })
                        }
                        emails.splice(0, 1);
                        resolve(checkEmail(emails, valid, invalid));
                    })

            } catch (error) {
                console.log(error)
                reject(error)
            }
        })
    }

    route.post('/emailExistSingle', middlewares.emailExistSingleValidate, async (req: any, res: Response) => {
        const logger = Container.get('logger') as any;
        try {
            const emailId = req.body.email;
            emailExistence.check(emailId, function (error: any, response: any) {
                res.status(200).json({
                    status: true,
                    message: response ? 'Valid Email' : 'InValid Email',
                    data: { response }
                })
            });

        } catch (e) {
            logger.error(' error : %o', e);

            return res.status(400).json({
                status: false,
                message: e.message
            })
        }
    })
};
