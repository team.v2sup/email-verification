import dotenv from 'dotenv';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const envFound = dotenv.config();

if (envFound.error) {
  throw new Error(" Unable to find .env file !!!");
}

const envData: any = envFound.parsed;

export default {

  objectCount : envData.OBJECT_COUNT,

  emailCount : envData.EMAIL_COUNT,

  nodeEnv: envData.NODE_ENV,

  port: parseInt(envData.PORT, 10),

  databaseURL: envData.MONGODB_URI,

  api: {
    prefix: '/notification',
  },

  swagger: {
    prefix: '/swagger',
  },

  logs: {
    level: 'silly',
  },

} as any;