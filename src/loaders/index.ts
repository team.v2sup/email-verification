import expressLoader from './express';
import mongoose from 'mongoose';
import Logger from './loggers';
import config from '../config';
import dependencyInjectorLoader from './dependencyInjectorLoader';

export default async ({ expressApp } : any) => {

    const connection = mongoose.connect(config.databaseURL , { 
        useNewUrlParser : true,
        useUnifiedTopology : true,
        useCreateIndex : true,
        useFindAndModify : false
    })

    Logger.info(' Dependency injector loaded');
    
    //await dependencyInjectorLoader();
    dependencyInjectorLoader();

    //await expressLoader({ app : expressApp });
    expressLoader({ app : expressApp });

    Logger.info('Express Loaded');

}