import express, { urlencoded } from 'express';
import cors from 'cors';
import config from '../config';
import routes from '../apis';
import swagger from '../apis/swagger';
import fileUpload from 'express-fileupload';


export default ({ app } : { app : express.Application }) => {

    app.get('/status', (req , res) => {
        res.status(200).end();
    });

    // what is this for... head
    app.head('/status' , (req , res) => {
        res.status(200).end();
    })

    // what is this for...
    app.enable('trust proxy');
    
    // adding file-upload //
    app.use(fileUpload());

    app.use(cors({ origin : true }));

    app.use(express.json({ limit : '5mb'}));

    app.use(express.urlencoded({ limit : '5mb'}));

    app.use(config.api.prefix , routes());
    app.use(config.swagger.prefix , swagger());
    // swagger url
    
    app.use((req , res , next) => {
        // const err :any = new Error('not Found');
        // // error err.status is not a function... //
        // //err.status(400);
        // next(err);
        res.status(400).json({
            status : false,
            err : 'Page Not Found'
        })
    })
}