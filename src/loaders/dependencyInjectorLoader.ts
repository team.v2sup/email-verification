import { Container } from 'typedi';
import LoggerInstance from './loggers';

export default () => {
    try{

        Container.set('logger' , LoggerInstance);
    
    }catch(e){
        LoggerInstance.error(' Error on dependency injector loader ' ,e);
        throw e;
    }
}

