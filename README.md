## Email Verification Service

### Steps to run :-

1. npm i
2. Create .env file
3. Copy this in .env file.

    -NODE_ENV=development
    -PORT=10002

    -MONGODB_URI=mongodb://localhost/notifications

    -EMAIL_COUNT=6000

2. nodemon .

3. Open Browser http://localhost:10002/swagger
